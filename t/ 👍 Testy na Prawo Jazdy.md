# ![](https://cdn.softexe.net/static/icon/c/testy-na-prawo-jazdy-9472.png) Testy na Prawo Jazdy 2.1.1.7 - Free Download

> Driving License tests is an application that helps in preparing for the ABCDTP category driving license.

[![Testy na Prawo Jazdy](https://gallery.dpcdn.pl/imgc/Tools/86857/g_-_420x350_1.5_-_x56659b9c-e28d-4a29-9488-894525e40e85.jpg)](https://softexe.net/win/education-science/other/testy-na-prawo-jazdy:adgb.html)

The program is based on a database approved by the Ministry of Infrastructure and Construction, which provides a full set of examination questions that are used during theoretical tests in WORDs. Driving License tests offers 2 modes of action - Science and Exam. The first one allows you to familiarize yourself with all the questions that you can come across during the state exam, while the second one simulates the exam - the user has to answer 32 random selected questions.
 
 The Driving Test application is available in 3 languages ​​(Polish, English and German), which can be successfully used by foreigners during preparations for the Polish driving test.


- **Update:** Dec 12 2018
- **File size:** 0.87 MB

[![Download](https://cdn.softexe.net/static/img/download.png)](https://softexe.net/win/education-science/other/testy-na-prawo-jazdy:adgb.html)

